FROM node:10.16-alpine

WORKDIR /app

COPY . /app

RUN ["npm", "install"]

ENTRYPOINT ["/usr/local/bin/npm"]
CMD ["start"]