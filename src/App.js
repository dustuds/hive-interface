import React from "react";
import "./App.css";
import QueryForm from "./UIComponents/QueryForm";

function App() {
  return (
    <div className="App">
      <QueryForm />
    </div>
  );
}

export default App;
