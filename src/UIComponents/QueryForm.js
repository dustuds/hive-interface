import React from "react";
import { Button, InputGroup, FormControl } from "react-bootstrap";
import "./QueryForm.css";

const QueryForm = () => {
  const exportQuery = () => {
    const queryFile = document.createElement("a");
    const file = new Blob([document.getElementById("queryInput").value]);
    queryFile.href = URL.createObjectURL(file);
    queryFile.download = "queryFile.txt";
    document.body.appendChild(queryFile);
    queryFile.click();

    document.getElementById("resultsDiv").innerHTML = document.getElementById(
      "queryInput"
    ).value;
  };

  return (
    <div className="center">
      <InputGroup controlId="queryForm">
        <InputGroup.Prepend className="qf">
          <InputGroup.Text>Query:</InputGroup.Text>
          <FormControl as="textarea" aria-label="text here" id="queryInput" />
        </InputGroup.Prepend>
      </InputGroup>
      <Button variant="primary" onClick={exportQuery}>
        Submit
      </Button>
      <div id="resultsDiv" />
    </div>
  );
};

export default QueryForm;
