## Use this to build image in the root directory (.) of this proj (where Dockerfile is located)

`docker build -t <tag-name> .`

## To run the container provide name of image and port we want react app to be accessible on

`docker run -p 8080:80 <tag-name>`
